<!DOCTYPE html>

<head>
    <title>Sign In</title>
</head>
<h1> Welcome ! </h1>
<body>
    <form method="post" action="{{ action('UsersController@store') }}">
        @csrf
        <fieldset>
            <legend>Enter your personal information</legend>
            <p>
                <label for="name">Name</label>
                <input type="text" name="name" id="name" placeholder="Ex : Gloria" maxlength="50" />

            </p>

            <p>
                <label for="email">Email</label>
                <input type="email" name="email" id="email" placeholder="Ex : example@hello.com" maxlength="50"  />

            </p>

            <p>
                <label for="password">Password</label>
                <input type="password" name="password" id="password" placeholder="12345678" minlength="8" maxlength="50"  />

            </p>
            <p>
                <input type="submit" value="Register" />

            </p>
        </fieldset>

</body>
