<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';
    protected $guarded = []; 
    const CREATED_AT = 'email_verified_at';
}
